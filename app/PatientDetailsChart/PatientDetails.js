'use strict';

angular.module('PatientDetails', ['DataExchange', "n3-line-chart"])

.directive('patientDetailChart', function() {
    return {
        scope: true,
        restrict: 'AE',
        replace: 'true',
        templateUrl: 'PatientDetailsChart/patient-detail.html'
    };
})


.controller('PatientDetailsChartCtrl', function($scope, sharedPatientSelection) {
        $scope.data = {};
        $scope.selectedPatient = {};
        $scope.isInit = true;

        // this gets called when the user selects a patient
        $scope.$on("handleSelectionBroadcast", function(){
            $scope.selectedPatient = sharedPatientSelection.selected;
            updateChart();
            $scope.isInit = false;
        });

        $scope.getName = function(patient){
            return patient.firstname + " " + patient.surname;
        };

        /**
         * Updates the chart based on the new data
         * @param patientData
         */
        var updateChart = function() {

            $scope.data = $scope.selectedPatient.data;

            // set the configuration for the bloodpressure chart
            $scope.bloodpressureOptions = {
                series: [
                    {
                        axis: "y",
                        dataset: "blood_pressure",
                        key: "systole",
                        label: "Systole in mmHg",
                        color: "#1f77b4",
                        type: ['line', 'dot'],
                        id: 'mySeries0'
                    },
                    {
                        axis: "y",
                        dataset: "blood_pressure",
                        key: "diastole",
                        label: "Diastole in mmHg",
                        color: "hsla(88, 48%, 48%, 1)",
                        type: ["line", 'dot'],
                        id: "mySeries1"
                    }
                ],
                axes: {x: {key: "date", type: "date"},
                       y: {min: 20, max: 220} }
            };

            // set the configuration for the cholesterin chart
            $scope.cholesterinOptions = {
                series: [
                    {
                        axis: "y",
                        dataset: "cholesterin",
                        key: "value",
                        label: "Cholesterin in mg/dl",
                        color: "#1f77b4",
                        type: ['line', 'dot'],
                        id: 'mySeries2'
                    }
                ],
                axes: {x: {key: "date", type: "date"},
                    y: {min: 30, max: 350} }
            };

            // set the configuration for the cholesterin chart
            $scope.bsaOptions = {
                series: [
                    {
                        axis: "y",
                        dataset: "BSA",
                        key: "value",
                        label: "Body Surface Area in m2",
                        color: "#1f77b4",
                        type: ['line', 'dot'],
                        id: 'mySeries3'
                    }
                ],
                axes: {x: {key: "date", type: "date"},
                    y: {min: 0.5, max: 3} }
            };
        }
});