/**
 * Serves the mocked patients, including their medical data
 * @type {angular.Module|*}
 */
var jsonService = angular.module('JSONServices', []);

jsonService.factory('PatientsDataService', function($http) {
        return {
            getAllData: function() {
                var promise = $http({method:'GET', url:'data.json'})
                    .success(function (data, status, headers, config) {

                        // parse the date string (json) and make a js Date out of it
                        // n3-chart does not understand strings ...
                        data.patients.forEach(function(patient){
                           for(var dataset in patient.data) {
                                patient.data[dataset].forEach(function(datasetEntry){
                                    datasetEntry.date = new Date(datasetEntry.date);
                                })
                            }
                        });

                        return data.patients;
                    })
                    .error(function (data, status, headers, config) {
                        return {"status": false};
                    });
                return promise;
            }
        }
});