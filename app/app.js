'use strict';

angular.module('medidataApp', ["PatientList", "PatientDetails"]);

/**
 * Handles the data transfer between views (eg. selection of a patient)
 */
angular.module('DataExchange', [])
.factory('sharedPatientSelection', function($rootScope) {
    var shared = {};

    shared.selected = {};

    shared.setSelection = function(patient){
        shared.selected = patient;
        shared.broadcastSelection();
    };

    shared.broadcastSelection = function(){
        $rootScope.$broadcast("handleSelectionBroadcast");
    };

    return shared;
});
