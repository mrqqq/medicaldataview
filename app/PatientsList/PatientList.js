'use strict';

angular.module('PatientList', ["JSONServices", "DataExchange", "ngAnimate"])

.controller('PatientListCtrl', function($scope, sharedPatientSelection, PatientsDataService) {
        PatientsDataService.getAllData().then(function(promise) {
            console.log(promise);
            $scope.patients = promise.data.patients;
        });

        $scope.selectionHandler = function (patient){
            sharedPatientSelection.setSelection(patient);
        };
})

.directive('patientListView', function() {
    return {
        scope: true,
        restrict: 'AE',
        replace: 'true',
        templateUrl: 'PatientsList/patient-list.html'
    };
});