'use strict';

describe('Medical Data View', function() {
  beforeEach(function() {
    browser.get('http://localhost:8000/app/');
  });

  it('should display 4 selectable patients', function(){
    element.all(by.repeater('patient in patients')).first().click();
    expect(element.all(by.repeater('patient in patients')).count()).toEqual(4);
  });

  it('should render the chart when a patient is selected', function() {
    element.all(by.repeater('patient in patients')).first().click();
    expect(element(by.css('chart')).isPresent()).toBeFalsy();
  });

  it('should display the name of the patient on top of the charts view', function() {
    element.all(by.repeater('patient in patients')).first().click();
    expect(element(by.binding('getName(selectedPatient)')).getText())
        .toBe(element.all(by.repeater('patient in patients')).first().getText());
  });
});
