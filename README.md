

```
npm start
```
Starts the application on port 8000.

```
npm run update-webdriver
```

This will download and install the latest version of the stand-alone WebDriver tool.


```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.
