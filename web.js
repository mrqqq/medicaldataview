var port = Number(process.env.PORT || 8000);

var express = require('express');
var app = express();

app.use("/app", express.static(__dirname + '/app'));
app.use("/node_modules/d3/", express.static(__dirname + '/node_modules/d3'));
app.use("/node_modules/n3-charts/build", express.static(__dirname + '/node_modules/n3-charts/build/'));
var server = app.listen(port, function() {
    console.log('Listening on port %d', server.address().port);
});